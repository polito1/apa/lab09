/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 12/08/2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct rappresentante una tessera.
typedef struct {
    char cVert;
    int vVert;
    char cOriz;
    int vOriz;
} tessera;

// Struct rappresentante una rappresentazione della tessera nella candidata a soluzione.
typedef struct {
    int index;
    int rotation;
} tesseraRappr;

// Definisco le funzioni che andrò ad utilizzare.
void readFile(tessera **tiles, tesseraRappr ***board, int *tilesNumber, int *nr, int *nc);
int isCellFree(tesseraRappr tileRapp);
int getPoints(tessera *tiles, tesseraRappr **board, int nr, int nc);
char getTileColor(tessera tile, int rotation, char orientation);
int getTileValue(tessera tile, int rotation, char orientation);
int getMaxBoard(tessera *tiles, tesseraRappr **board, int tilesNumber, int nr, int nc);
void disp_sempl(tessera *val, tesseraRappr **sol, int * marks, int n, int k, int pos, int nr, int nc, int *maxPoints, tesseraRappr **bestSol);
void matrixCopy(tesseraRappr **src, tesseraRappr **dest, int nr, int nc);
void getMatrixIndexes(int pos, int *r, int *c, int nc);
void malloc2d(tesseraRappr ***matrix, int nr, int nc);
void printSolution(tesseraRappr **sol, int nr, int nc);

int main() {
    tessera *tiles;
    tesseraRappr **board;
    int tilesNumber, nr, nc;

    readFile(&tiles, &board, &tilesNumber, &nr, &nc);

    printf("Punteggio massimo: %d", getMaxBoard(tiles, board, tilesNumber, nr, nc));

    // Libero l'array.
    free(tiles);

    // Libero ogni riga della matrice.
    for (int i = 0; i < nr; i++) {
        free(board[i]);
    }

    // Libero la matrice.
    free(board);

    return 0;
}

int getMaxBoard(tessera *tiles, tesseraRappr **board, int tilesNumber, int nr, int nc)
{
    int *marks, k = nr * nc, maxPoints = -1;
    tesseraRappr tileRapp, **bestSol;
    marks = (int *) calloc(tilesNumber, sizeof(int));
    malloc2d(&bestSol, nr, nc);

    // Segno in marks le tessere già utilizzate nella soluzione parziale presa da file.
    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nc; j++) {
            tileRapp = board[i][j];

            if (!isCellFree(tileRapp)) {
                marks[tileRapp.index] = 1;
            }
        }
    }

    // Richiamo la funzione ricorsiva.
    disp_sempl(tiles, board, marks, tilesNumber, k, 0, nr, nc, &maxPoints, bestSol);

    // Stampo la soluzione migliore.
    printSolution(bestSol, nr, nc);

    // Ritorno il punteggio massimo.
    return maxPoints;
}

// Leggo da file i dati e li salvo in opportune strutture.
void readFile(tessera **tiles, tesseraRappr ***board, int *tilesNumber, int *nr, int *nc)
{
    FILE *tilesFile, *boardFile;
    tessera tile, *tilesTmp;
    tesseraRappr tileRapp, **boardTmp;
    *nr = -1;
    *nc = -1;
    *tilesNumber = -1;

    tilesFile = fopen("tiles.txt", "r");

    if (tilesFile == NULL) {
        return;
    }

    fscanf(tilesFile, "%d\n", tilesNumber);

    tilesTmp = (tessera *) malloc(*tilesNumber * sizeof(tessera));

    for (int i = 0; i < *tilesNumber; i++) {
        fscanf(tilesFile, "%c %d %c %d\n", &tile.cOriz, &tile.vOriz, &tile.cVert, &tile.vVert);

        tilesTmp[i] = tile;
    }

    *tiles = tilesTmp;

    fclose(tilesFile);

    boardFile = fopen("board.txt", "r");

    if (boardFile == NULL) {
        return;
    }

    fscanf(boardFile, "%d %d", nr, nc);

    boardTmp = (tesseraRappr **) malloc(*nr * sizeof(tesseraRappr *));

    for (int i = 0; i < *nr; i++) {
        boardTmp[i] = (tesseraRappr *) malloc(*nc * sizeof(tesseraRappr));

        for (int j = 0; j < *nc; j++) {
            fscanf(boardFile, "%d/%d", &tileRapp.index, &tileRapp.rotation);
            boardTmp[i][j] = tileRapp;
        }
    }

    *board = boardTmp;

    fclose(boardFile);
}

void disp_sempl(tessera *val, tesseraRappr **sol, int *marks, int n, int k, int pos, int nr, int nc, int *maxPoints, tesseraRappr **bestSol)
{
    int points, r, c;
    tesseraRappr tmp;

    // Se sono arrivato alla massima capienza.
    if (pos >= k) {
        // Calcolo il punteggio della soluzione corrente.
        points = getPoints(val, sol, nr, nc);
        // Se il punteggio è maggiore di quello massimo,
        if (points > *maxPoints) {
            // Aggiorno il punteggio e la soluzione migliore.
            *maxPoints = points;
            matrixCopy(sol, bestSol, nr, nc);
        }

        return;
    }

    for (int i = 0; i < n; i++) {
        tmp.index = i;
        if (marks[tmp.index] == 0) {
            getMatrixIndexes(pos, &r, &c, nc);

            if (isCellFree(sol[r][c])) {
                marks[tmp.index] = 1;
                tmp.rotation = 0;

                // Inserisco la tessera non ruotata.
                sol[r][c] = tmp;
                disp_sempl(val, sol, marks, n, k, pos + 1, nr, nc, maxPoints, bestSol);

                // Inserisco la tessera ruotata.
                tmp.rotation = 1;
                sol[r][c] = tmp;
                disp_sempl(val, sol, marks, n, k, pos + 1, nr, nc, maxPoints, bestSol);

                // Backtracking.
                marks[tmp.index] = 0;
                sol[r][c].index = - 1;
            } else {
                disp_sempl(val, sol, marks, n, k, pos + 1, nr, nc, maxPoints, bestSol);
            }
        }
    }
}

int getPoints(tessera *tiles, tesseraRappr **board, int nr, int nc)
{
    int sum = 0, partialSum, currentValue;
    char currentColor, prevColor;
    tessera currentTile, prevTile;

    for (int i = 0; i < nr; i++) {
        partialSum = 0;

        for (int j = 0; j < nc; j++) {
            currentTile = tiles[board[i][j].index];

            currentValue = getTileValue(currentTile, board[i][j].rotation, 'o');
            currentColor = getTileColor(currentTile, board[i][j].rotation, 'o');

            if (j == 0) {
                partialSum += currentValue;
                continue;
            }

            prevTile = tiles[board[i][j - 1].index];

            prevColor = getTileColor(prevTile, board[i][j - 1].rotation, 'o');

            // Se sono dello stesso colore devo sommare alla somma parziale il valore corrente.
            if (currentColor == prevColor) {
                partialSum += currentValue;
            } else {
                partialSum = 0;
                break;
            }
        }

        sum += partialSum;
    }

    for (int j = 0; j < nc; j++) {
        partialSum = 0;

        for (int i = 0; i < nr; i++) {
            currentTile = tiles[board[i][j].index];

            currentValue = getTileValue(currentTile, board[i][j].rotation, 'v');
            currentColor = getTileColor(currentTile, board[i][j].rotation, 'v');

            if (i == 0) {
                partialSum += currentValue;
                continue;
            }

            prevTile = tiles[board[i - 1][j].index];

            prevColor = getTileColor(prevTile, board[i - 1][j].rotation, 'v');

            // Se sono dello stesso colore devo sommare alla somma parziale il valore corrente.
            if (currentColor == prevColor) {
                partialSum += currentValue;
            } else {
                partialSum = 0;
                break;
            }
        }

        sum += partialSum;
    }

    return sum;
}

char getTileColor(tessera tile, int rotation, char orientation)
{
    // Voglio il valore orizzontale o quello verticale?
    int orizzontale = orientation == 'o';

    // Voglio l'orizzontale.
    if (orizzontale) {
        // La tessera è ruotata?
        if (rotation) {
            return tile.cVert;
        }
        return tile.cOriz;
    }
    // Voglio il verticale.
    else {
        // La tessera è ruotata?
        if (rotation) {
            return tile.cOriz;
        }
        return tile.cVert;
    }
}

int getTileValue(tessera tile, int rotation, char orientation)
{
    // Voglio il valore orizzontale o quello verticale?
    int orizzontale = orientation == 'o';

    // Voglio l'orizzontale.
    if (orizzontale) {
        // La tessera è ruotata?
        if (rotation) {
            return tile.vVert;
        }
        return tile.vOriz;
    }
        // Voglio il verticale.
    else {
        // La tessera è ruotata?
        if (rotation) {
            return tile.vOriz;
        }
        return tile.vVert;
    }
}

int isCellFree(tesseraRappr tileRapp)
{
    return tileRapp.index == - 1;
}

void matrixCopy(tesseraRappr **src, tesseraRappr **dest, int nr, int nc)
{
    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nc; j++) {
            dest[i][j] = src[i][j];
        }
    }
}

void getMatrixIndexes(int pos, int *r, int *c, int nc)
{
    if (pos < nc) {
        *r = 0;
        *c = pos;
        return;
    }

    *r = pos / nc;
    *c = pos % nc;
}

void malloc2d(tesseraRappr ***matrix, int nr, int nc)
{
    tesseraRappr **m;

    m = (tesseraRappr **) malloc(nr * sizeof(tesseraRappr*));

    for (int i = 0; i < nr; i++) {
        m[i] = (tesseraRappr *) malloc(nc * sizeof(tesseraRappr));
    }

    *matrix = m;
}

void printSolution(tesseraRappr **sol, int nr, int nc)
{
    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nc; j++) {
            printf("%d/%d", sol[i][j].index, sol[i][j].rotation);
            printf("\t");
        }

        printf("\n");
    }
}
