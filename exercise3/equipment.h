/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */

#ifndef EXERCISE3_EQUIPMENT_H
#define EXERCISE3_EQUIPMENT_H

#define STRING_LENGTH 50

#define STATS_LENGTH 6
#define STATS_NAME_LENGTH 3
#define HP_INDEX 0
#define MP_INDEX 1
#define ATK_INDEX 2
#define DEF_INDEX 3
#define MAG_INDEX 4
#define SPR_INDEX 5

// Struttura rappresentante un equipaggiamento.
typedef struct {
    char nome[STRING_LENGTH + 1];
    char tipo[STRING_LENGTH + 1];
    int stat[STATS_LENGTH];
} inv_t;

// Struttura rappresentante l'inventario.
typedef struct {
    inv_t *vettInv;
    int nInv;
    int maxInv;
} tabInv_t;

// Struttura rappresentante gli equipaggiamenti.
typedef struct {
    int inUso;
    inv_t **vettEq;
} tabEquip_t;

// Definisco le funzioni dell'inventario.
void loadObjects(tabInv_t *tab);
inv_t *searchEquipment(tabInv_t *t, char *name);
void printEquipmentStats(tabInv_t *t);

#endif //EXERCISE3_EQUIPMENT_H
