/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "player.h"

void loadPlayers(tabPg_t *tab)
{
    char fileName[STRING_LENGTH + 1];
    FILE *fp;
    pg_t player;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    // Gestione degli errori riguardanti il file.
    if (fp == NULL) {
        return;
    }

    for (int i = 0; !feof(fp); i++) {
        fscanf(
                fp,
                "%s %s %s %d %d %d %d %d %d",
                player.codice,
                player.nome,
                player.classe,
                &player.stat.hp,
                &player.stat.mp,
                &player.stat.atk,
                &player.stat.def,
                &player.stat.mag,
                &player.stat.spr
        );

        // Creo un equipaggiamento di default.
        player.equip = malloc(sizeof(tabEquip_t *));
        player.equip->inUso = -1;
        player.equip->vettEq = (inv_t **) malloc(MAX_EQUIPS * sizeof(inv_t *));

        // Inserisco il personaggio nella lista.
        insert(&tab, player);
        tab->nPg += 1;
    }
}

void addPlayer(tabPg_t *tab)
{
    pg_t tmp;

    printf("Inserire le informazioni (codice nome classe hp mp atk def mag spr): ");
    scanf(
            "%s %s %s %d %d %d %d %d %d",
            tmp.codice,
            tmp.nome,
            tmp.classe,
            &tmp.stat.hp,
            &tmp.stat.mp,
            &tmp.stat.atk,
            &tmp.stat.def,
            &tmp.stat.mag,
            &tmp.stat.spr
    );

    // Creo un equipaggiamento di default.
    tmp.equip->inUso = -1;

    // Inserisco il personaggio nella lista.
    insert(&tab, tmp);
    tab->nPg += 1;
}

void removePlayer(tabPg_t *tab)
{
    char code[CODICE_LENGTH + 1];
    struct nodoPg_t *tmp;

    printf("Inserire il codice dell'elemento da eliminare: ");
    scanf("%s", code);

    // Elimino il personaggio dalla lista.
    tmp = deletePlayer(&tab, code);

    if (tmp != NULL) {
        printf("Elemento eliminato!\n");
        // Posso deallocare la porzione di memoria che era occupata dall'elemento.
        free(*tmp->personaggio.equip->vettEq);
        free(tmp);
        return;
    }

    printf("Elemento non trovato!\n");
}

// Inserisce un nodo in testa alla lista.
void insert(tabPg_t **tab, pg_t player)
{
    if ((*tab)->headPg == NULL) {
        newNode(player, NULL);
        (*tab)->headPg = (*tab)->tailPg;
    }
    (*tab)->headPg = newNode(player, (*tab)->headPg);
}

// Crea un nuovo nodo e lo ritorna.
struct nodoPg_t *newNode(pg_t player, struct nodoPg_t *next)
{
    struct nodoPg_t *node;
    node = (struct nodoPg_t *) malloc(sizeof(struct nodoPg_t));

    if (node == NULL) {
        return NULL;
    }

    node->personaggio = player;
    node->next = next;

    return node;
}

// Cerca un nodo dato il codice personaggio.
struct nodoPg_t *searchPlayer(tabPg_t *tab, char code[CODICE_LENGTH + 1])
{
    struct nodoPg_t *tmp;

    for (tmp = tab->headPg; tmp != NULL && strcmp(tmp->personaggio.codice, code) != 0; tmp = tmp->next);

    return tmp;
}

// Elimina un nodo dalla lista.
struct nodoPg_t *deletePlayer(tabPg_t **tab, char code[CODICE_LENGTH + 1])
{
    struct nodoPg_t *tmp;
    struct nodoPg_t *before;

    if ((*tab)->headPg == NULL) {
        return NULL;
    }

    tmp = (*tab)->headPg;
    before = NULL;

    while (tmp != NULL && strcmp(code, tmp->personaggio.codice) != 0) {
        before = tmp;
        tmp = tmp->next;
    }

    if (tmp == NULL) {
        return NULL;
    }

    before->next = tmp->next;

    return tmp;
}

// Stampa le statistiche del personaggio.
void printPlayer(pg_t player)
{
    printf("%s %s %s ", player.codice, player.nome, player.classe);

    // Se una statistica è < 0, stampa 0 come richiesto.
    printf(
            "hp: %d, mp: %d, atk: %d, def: %d, mag: %d, spr: %d ",
            player.stat.hp >= 0 ? player.stat.hp : 0,
            player.stat.mp >= 0 ? player.stat.mp : 0,
            player.stat.atk >= 0 ? player.stat.atk : 0,
            player.stat.def >= 0 ? player.stat.def : 0,
            player.stat.mag >= 0 ? player.stat.mag : 0,
            player.stat.spr >= 0 ? player.stat.spr : 0
    );

    printf("\n");
}

void getStats(tabPg_t *tab)
{
    char playerCode[CODICE_LENGTH + 1];
    pg_t tmp;
    struct nodoPg_t *node;

    printf("Inserire il codice del personaggio: ");
    scanf("%s", playerCode);

    node = searchPlayer(tab, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste.\n");
        return;
    }

    // Creo un personaggio temporaneo per maneggiare le statistiche senza sovrascrivere quelle originali.
    tmp = node->personaggio;

    for (int i = 0; i < node->personaggio.equip->inUso; i++) {
        tmp.stat.hp += node->personaggio.equip->vettEq[i]->stat[HP_INDEX];
        tmp.stat.mp += node->personaggio.equip->vettEq[i]->stat[MP_INDEX];
        tmp.stat.atk += node->personaggio.equip->vettEq[i]->stat[ATK_INDEX];
        tmp.stat.def += node->personaggio.equip->vettEq[i]->stat[DEF_INDEX];
        tmp.stat.mag += node->personaggio.equip->vettEq[i]->stat[MAG_INDEX];
        tmp.stat.spr += node->personaggio.equip->vettEq[i]->stat[SPR_INDEX];
    }

    // Stampo le statistiche.
    printPlayer(tmp);
}

void addEquip(tabPg_t *tabPg, tabInv_t *tabInv)
{
    inv_t *equip = NULL;
    struct nodoPg_t *node;
    char equipName[STRING_LENGTH + 1], playerCode[CODICE_LENGTH + 1];

    printf("Inserire il nome dell'equipaggiamento da aggiungere: ");
    scanf("%s", equipName);

    // Ricerca lineare dell'equipaggiamento nell'inventario.
    for (int i = 0; i < tabInv->nInv; i++) {
        if (strcmp(tabInv->vettInv[i].nome, equipName) == 0) {
            equip = &tabInv->vettInv[i];
        }
    }

    if (equip == NULL) {
        printf("L'equipaggiamento cercato non esiste.");
        return;
    }

    printf("Inserire il codice del personaggio a cui aggiungere l'equipaggiamento: ");
    scanf("%s", playerCode);

    // Ricerca del personaggio dato il codice.
    node = searchPlayer(tabPg, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste.");
        return;
    }

    if (node->personaggio.equip->inUso >= MAX_EQUIPS) {
        printf("Il personaggio dispone già di 8 equipaggiamenti, eliminarne alcuni e riprovare.\n");
        return;
    }

    node->personaggio.equip->vettEq[node->personaggio.equip->inUso] = equip;
    node->personaggio.equip->inUso += 1;
}

void removeEquip(tabPg_t *tabPg)
{
    char equipName[STRING_LENGTH + 1], playerCode[CODICE_LENGTH + 1];
    int index = -1;
    struct nodoPg_t *node;

    printf("Inserire il nome dell'equipaggiamento da rimuovere: ");
    scanf("%s", equipName);

    printf("Inserire il codice del personaggio a cui aggiungere l'equipaggiamento: ");
    scanf("%s", playerCode);

    node = searchPlayer(tabPg, playerCode);

    if (node == NULL) {
        printf("Il personaggio cercato non esiste. \n");
        return;
    }

    // Ricerca dell'equipaggiamento tra quelli del personaggio.
    for (int i = 0; i < node->personaggio.equip->inUso; i++) {
        if (strcmp(node->personaggio.equip->vettEq[i]->nome, equipName) == 0) {
            index = i;
        }
    }

    if (index == -1) {
        printf("Il personaggio non contiene l'equipaggiamento cercato. \n");
        return;
    }

    // Shift del vettore degli equipaggiamenti verso sinistra.
    for (int j = index; j < node->personaggio.equip->inUso - 1; j++) {
        node->personaggio.equip->vettEq[j] = node->personaggio.equip->vettEq[j + 1];
    }

    node->personaggio.equip->inUso -= 1;
}
