/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */

#include "equipment.h"

#ifndef EXERCISE3_PLAYER_H
#define EXERCISE3_PLAYER_H

#define CODICE_LENGTH 6
#define MAX_EQUIPS 8

// Struttura rappresentante le statistiche.
typedef struct {
    int hp;
    int mp;
    int atk;
    int def;
    int mag;
    int spr;
} stat_t;

// Struttura rappresentante un personaggio.
typedef struct {
    char codice[CODICE_LENGTH + 1];
    char nome[STRING_LENGTH + 1];
    char classe[STRING_LENGTH + 1];
    tabEquip_t *equip;
    stat_t stat;
} pg_t;

// Struttura rappresentante un nodo nella lista.
struct nodoPg_t {
    pg_t personaggio;
    struct nodoPg_t *next;
};

// Struttura rappresentante una lista.
typedef struct {
    struct nodoPg_t *headPg;
    struct nodoPg_t *tailPg;
    int nPg;
} tabPg_t;

// Definisco le funzioni del player.
void loadPlayers(tabPg_t *tab);
void addPlayer(tabPg_t *tab);
void removePlayer(tabPg_t *tab);
void printPlayer(pg_t player);
void getStats(tabPg_t *tab);
void addEquip(tabPg_t *tabPg, tabInv_t *tabInv);
void removeEquip(tabPg_t *tabPg);
void insert(tabPg_t **tab, pg_t player);
struct nodoPg_t *searchPlayer(tabPg_t *tab, char code[CODICE_LENGTH + 1]);
struct nodoPg_t *deletePlayer(tabPg_t **tab, char code[CODICE_LENGTH + 1]);
struct nodoPg_t *newNode(pg_t player, struct nodoPg_t *next);

#endif //EXERCISE3_PLAYER_H
