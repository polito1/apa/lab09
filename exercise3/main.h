/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */

#include "player.h"

#ifndef EXERCISE3_MAIN_H
#define EXERCISE3_MAIN_H

#define COMMAND_LENGTH 25
#define COMMANDS_NUMBER 9

// Enum dei comandi disponibili.
typedef enum comando_e{
    carica_lista,
    carica_oggetti,
    aggiungi_personaggio,
    elimina_personaggio,
    aggiungi_equipaggiamento,
    rimuovi_equipaggiamento,
    stampa_statistiche,
    stampa_equipaggiamento,
    fine
} comando;

int leggiComando();

#endif //EXERCISE3_MAIN_H
