/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */


#include <stdio.h>
#include <string.h>
#include "player.h"
#include "main.h"

int main() {
    tabPg_t listaPersonaggi;
    tabInv_t listaOggetti;
    int continua = 1;

    // Imposto i parametri default.
    listaPersonaggi.nPg = 0;
    listaPersonaggi.headPg = NULL;
    listaPersonaggi.tailPg = NULL;
    listaOggetti.nInv = 0;

    // Menu utente.
    while (continua) {
        switch (leggiComando()) {
            case carica_lista:
                loadPlayers(&listaPersonaggi);
                break;
            case carica_oggetti:
                loadObjects(&listaOggetti);
                break;
            case aggiungi_personaggio:
                addPlayer(&listaPersonaggi);
                break;
            case elimina_personaggio:
                removePlayer(&listaPersonaggi);
                break;
            case aggiungi_equipaggiamento:
                addEquip(&listaPersonaggi, &listaOggetti);
                break;
            case rimuovi_equipaggiamento:
                removeEquip(&listaPersonaggi);
                break;
            case stampa_statistiche:
                getStats(&listaPersonaggi);
                break;
            case stampa_equipaggiamento:
                printEquipmentStats(&listaOggetti);
                break;
            default:
                continua = 0;
        }
    }
    return 0;
}

int leggiComando()
{
    char comandi[COMMANDS_NUMBER][COMMAND_LENGTH + 1] = {
            "carica_lista",
            "carica_oggetti",
            "aggiungi_personaggio",
            "elimina_personaggio",
            "aggiungi_equipaggiamento",
            "rimuovi_equipaggiamento",
            "stampa_statistiche",
            "stampa_equipaggiamento",
            "fine"
    };
    char comandoTmp[COMMAND_LENGTH + 1];
    int i = 0;

    printf("Inserire un comando (carica_lista, carica_oggetti, aggiungi_personaggio, elimina_personaggio, aggiungi_equipaggiamento, rimuovi_equipaggiamento, stampa_statistiche, stampa_equipaggiamento, fine): ");
    scanf("%s", comandoTmp);

    for (i = 0; i < COMMANDS_NUMBER && strcmp(comandoTmp, comandi[i]) != 0; i++);

    return i;
}
