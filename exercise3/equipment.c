/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/3/20
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "equipment.h"

void loadObjects(tabInv_t *tab)
{
    char fileName[STRING_LENGTH + 1];
    FILE *fp;
    int objectsNumber;
    inv_t tmp;

    printf("Inserire il nome del file: ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        return;
    }

    fscanf(fp, "%d", &objectsNumber);

    // Creo un vettore di equipaggiamenti.
    tab->vettInv = (inv_t *) malloc(objectsNumber * sizeof(inv_t));

    for (int i = 0; i < objectsNumber; i++) {
        fscanf(
                fp,
                "%s %s %d %d %d %d %d %d",
                tmp.nome,
                tmp.tipo,
                &tmp.stat[HP_INDEX],
                &tmp.stat[MP_INDEX],
                &tmp.stat[ATK_INDEX],
                &tmp.stat[DEF_INDEX],
                &tmp.stat[MAG_INDEX],
                &tmp.stat[SPR_INDEX]
        );

        tab->vettInv[i] = tmp;
        tab->nInv = tab->nInv + 1;
    }
}

inv_t *searchEquipment(tabInv_t *t, char *name)
{
    for (int i = 0; i < t->nInv; i++) {
        if (strcmp(t->vettInv[i].nome, name) == 0) {
            return &t->vettInv[i];
        }
    }

    return NULL;
}

void printEquipmentStats(tabInv_t *t)
{
    char name[STRING_LENGTH + 1];
    inv_t *equip;

    char stats_names[STATS_LENGTH][STATS_NAME_LENGTH + 1] = {
            "hp",
            "mp",
            "atk",
            "def",
            "mag",
            "spr"
    };

    printf("Inserire il nome dell'equipaggiamento da cercare: ");
    scanf("%s", name);

    equip = searchEquipment(t, name);

    if (equip == NULL) {
        printf("Nessun equipaggiamento trovato.");
        return;
    }

    printf("%s %s", equip->nome, equip->tipo);

    for (int i = 0; i < STATS_LENGTH; i++) {
        printf(" %s: %d", stats_names[i], equip->stat[i]);
    }

    printf("\n");
}