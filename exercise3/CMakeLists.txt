cmake_minimum_required(VERSION 3.17)
project(exercise3 C)

set(CMAKE_C_STANDARD 99)

add_executable(exercise3 main.c player.h equipment.h main.h player.c equipment.c)