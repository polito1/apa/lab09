/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 12/03/2020
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_STRING_LENGTH 50

// Definisco la struttura dati di riferimento.
typedef struct {
    int start;
    int end;
    int duration;
} att;

// Definisco le funzioni che implementerò.
void attSel(int n, att *v);
int readFromFile(att **v);
int validate(att *v, int dim);
void disp_sempl(int pos, att *val, att *sol, int *mark, int n, int k, int *found, int *bestSolValue, int currentValue, int *bestSolDim, att *bestSol);
void printSolution(att *sol, int dim);
int attCompare(att a, att b);
int validateMalloc(void *a, void *b, void *c, void *d);

int main() {
    att *v;
    int nVal;

    // Leggo da file e salvo il numero di valori letti.
    nVal = readFromFile(&v);

    // Richiamo la funzione core.
    attSel(nVal, v);

    // Dealloco il vettore istanziato precedentemente.
    free(v);
    return 0;
}

void attSel(int n, att *v)
{
    att *sol, *bestSol, *savedSol;
    int bestSolValue, found, *mark, bestSolDim = 0, maxValue = -1, maxDim = 0;

    // Allocazione dinamica dei vettori.
    sol = (att *) malloc(n * sizeof(att));
    bestSol = (att *) malloc(n * sizeof(att));
    savedSol = (att *) malloc(n * sizeof(att));
    mark = (int *) calloc(n, sizeof(int));

    // Controllo che tutte le malloc siano andate a buon fine.
    if (!validateMalloc(sol, bestSol, savedSol, mark)) {
        printf("Errore di allocazione.");
        return;
    }

    // Ciclo su ogni k.
    for (int i = 1; i <= n; i++) {
        found = 0;
        disp_sempl(0, v, sol, mark, n, i, &found, &bestSolValue, 0, &bestSolDim, bestSol);

        // Se ho trovato una soluzione ed ha valore maggiore di quella trovata in precedenza
        if (found && bestSolValue > maxValue) {
            maxValue = bestSolValue;
            maxDim = bestSolDim;

            // Salvo la soluzione massima con k = i.
            for (int j = 0; j < bestSolDim; j++) {
                savedSol[j] = bestSol[j];
            }
        }
    }

    // Stampo la soluzione trovata.
    printSolution(savedSol, maxDim);

    // Deallocazione dei vettori.
    free(sol);
    free(bestSol);
    free(mark);
}

void disp_sempl(int pos, att *val, att *sol, int *mark, int n, int k, int *found, int *bestSolValue, int currentValue, int *bestSolDim, att *bestSol)
{
    if (pos >= k) {
        // Se è una soluzione valida, la salvo nel vettore bestSol e aggiorno il valore di found.
        if (validate(sol, pos)) {
            *found = 1;
            if (currentValue > *bestSolValue) {
                *bestSolValue = currentValue;
                *bestSolDim = pos;

                for (int i = 0; i < pos; i++) {
                    bestSol[i] = sol[i];
                }
            }
        }

        return;
    }

    for (int i = 0; i < n; i++) {
        // Se il valore non è ancora stato inserito
        if (mark[i] == 0) {
            // Inserisco il valore
            sol[pos] = val[i];

            // Segno che il valore è già stato assegnato e aggiorno il peso della soluzione con il nuovo valore inserito.
            mark[i] = 1;
            currentValue += val[i].duration;

            // Chiamata ricorsiva.
            disp_sempl(pos + 1, val, sol, mark, n, k, found, bestSolValue, currentValue, bestSolDim, bestSol);

            // Backtracking.
            mark[i] = 0;
            currentValue -= val[i].duration;
        }
    }
}

int readFromFile(att **v)
{
    FILE *fp;
    char fileName[MAX_STRING_LENGTH + 1];
    int nVal;
    att tmp;

    printf("Inserire il nome del file (MAX 50 CARATTERI): ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        v = NULL;
        return -1;
    }

    fscanf(fp, "%d", &nVal);

    // Allocazione dinamica del vettore.
    *v = (att *) malloc(nVal * sizeof(att));

    // Inserisco i valori nel vettore.
    for (int i = 0; i < nVal && !feof(fp); i++) {
        fscanf(fp, "%d %d", &tmp.start, &tmp.end);
        tmp.duration = tmp.end - tmp.start;

        (*v)[i] = tmp;
    }

    // Chiudo il file e ritorno il numero di valori letti.
    fclose(fp);
    return nVal;
}

int validate(att *v, int dim)
{
    int compare;
    // Confronto ogni elemento del vettore per controllare che non ci siano attività incompatibili
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {
            // Evito di controllare un elemento con sè stesso.
            if (i == j) {
                continue;
            }

            // Controllo quale attività inizia prima.
            compare = attCompare(v[i], v[j]);
            // Se iniziano insieme, sicuramente sono incompatibili.
            if (compare == 0) {
                return 0;
            }

            // Se l'attività di indice i inizia prima.
            if (compare < 0) {
                // Controllo che le due attività non siano incompatibili.
                if (v[j].start < v[i].end || v[i].end > v[j].start) {
                    return 0;
                }
            }

            // Se l'attività di indice j inizia prima.
            if (compare > 0) {
                // Controllo che le due attività non siano incompatibili.
                if (v[i].start < v[j].end || v[j].end > v[i].start) {
                    return 0;
                }
            }
        }
    }

    return 1;
}

// Stampa la soluzione.
void printSolution(att *sol, int dim)
{
    int s = 0;

    for (int i = 0; i < dim; i++) {
        s += sol[i].duration;
        printf("start: %d, end: %d, duration: %d\n", sol[i].start, sol[i].end, sol[i].duration);
    }

    printf("Somma totale: %d\n", s);
}

// Confronto l'inizio delle due attività.
int attCompare(att a, att b)
{
    if (a.start == b.start) {
        return 0;
    }

    if (a.start < b.start) {
        return -1;
    }

    return 1;
}

// Questa funzione non porta alcun valore aggiunto a livello di funzionalità, ma aumenta di gran lunga la leggibilità
// del codice.
int validateMalloc(void *a, void *b, void *c, void *d)
{
    return (
        (a != NULL)
        && (b != NULL)
        && (c != NULL)
        && (d != NULL)
    );
}